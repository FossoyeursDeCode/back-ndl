
const express = require('express');
const router = express.Router();

const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://localhost:27017/";

/**
 * Récupére la storyId en fonction du userId
 * @param userId
 * @param db
 * @param callback
 */
function getUserByStoryId(userId, db, callback){
  db.db("fdclive").collection("users").find({ id : userId}).toArray(function(err, result) {
    if (err) throw err;
    callback(result[0]);
  });
}

/**
 * Renvoit un message en fonction de la question et de la réponse de l'utilisateur
 *
 * @param message
 * @param userId
 * @param storyId
 * @param storyType
 * @param bd
 * @param callback
 */
function laMoulinette(message, user, storyType, storyId, bd, callback) {
  if (storyType === 0){
    switch (storyId) {
      case 0 :
        storyInc(user.id,storyType,bd);

        callback({
          message : "Bonjour \" + user.name + \"\\n Tu es sur de pouvoir prendre le volant ?"
        });
        break;
      case 1:
        storyInc(user.id,storyType,bd);

        if (message = "oui")
          callback({message : "Tu as l'air sur de toi, faisons un test quand même. \n Testons d'abord ta réactivité !" });
        else
          callback({message : "Tu fais bien d'être là, let's go pour un test. \n D'abord un test de réactivité !" });

      case 2:
		storyInc(user.id,storyType,bd);
		//Dérouler test reaction

      case 3:
		storyInc(user.id,storyType,bd);


	  case 4:
		storyInc(user.id,storyType,bd);
		alcoTest(user.id);

	  case 5:
		storyInc(user.id,storyType,bd);
		if(user.data.isBourred)
			callback({message : "Ne prend pas le volant ! \n Ton taux d'alcoolémie est trop élevé !" });

	}
  }
}

/**
 *
 * @param message
 * @param listWord
 * @return {boolean}
 */
function messageContient(message, listWord){
  for(let i = 0 ; i<listWord.lenght ; i++){
    if(message.search(listWord[i]) > -1)
      return true;
  }
  return false;
}

/**
 * Incrémentation de la story id
 * @param storyType
 * @param userId
 */
function storyInc(userId, storyType, db){
  //db.db("fdclive").collection("users").update({...}, {$inc: {"answer.0.votes": 1}});

  if (storyType === 0){
    db.db("fdclive").collection("users").findOneAndUpdate(
      { id : userId },
      { $inc: { 'story.0': 1 } }
        );
  } else {
    db.db("fdclive").collection("users").findOneAndUpdate({
      query: { id : userId },
      update: { $inc: { 'story.1': 1 } }
    });
  }


}

function rndTxAlcool() {
	return Math.round(Math.random() * Math.pow(10,2)) / Math.pow(10,2);
}

function alcoTest(userId) {

	let maxTaux;

	if (apprenti)
		maxTaux = 0.2;
	else
		maxTaux = 0.5;

	let txAlcool = rndTxAlcool();

	console.log(txAlcool);

	if(txAlcool >= maxTaux){
		db.db("fdclive").collection("users").findOneAndUpdate(
			{ id : userId },
			{ data: { 'data.isBourred': true } }
		);
	}else{
		db.db("fdclive").collection("users").findOneAndUpdate(
			{ id : userId },
			{ data: { 'data.isBourred': false } }
		);
	}
}

//
router.route('/')
  .post((req, res) => {

    MongoClient.connect(url, function(err, db) {
      if (err) throw err;

      let out = {};

      let userId = req.body.userId;
      let storyId = 0;
      let message = req.body.message;
      message = String(message).toLowerCase();

      getUserByStoryId(userId, db, (resdb) => {
        laMoulinette(message,resdb,0,resdb.story[storyId],db, (data) => {
          db.close();
          out.message = data;
          out.userId = resdb.id;
          res.end(JSON.stringify(out));
        } );

      });



    });
  });

module.exports = router;


const express = require('express');
const router_message = require('./router_message');
const router_user = require('./router_user');
const bodyParser = require('body-parser');
const app = express();


// Middleware
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/message', router_message);

app.use('/user', router_user);

app.get('/', function (req, res) {
  res.send('Err');
});


app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
});